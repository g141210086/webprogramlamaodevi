//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebProgramlama.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tiyatro
    {
        public int TiyatroId { get; set; }
        public string Tiyatro_Adi { get; set; }
        public string Tiyatro_Aciklama { get; set; }
        public int Tiyatro_Kategori { get; set; }
    
        public virtual TiyatroKategori TiyatroKategori { get; set; }
    }
}
