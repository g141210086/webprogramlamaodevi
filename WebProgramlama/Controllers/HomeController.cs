﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebProgramlama.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Kitap()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Sinema()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Sanat()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Tiyatro()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Roportaj()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}